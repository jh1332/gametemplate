﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public enum EventType
    {
        TestEvent_0 = 0,
        TestEvent_1,
    }

    public enum PopupType
    {
        TestPopup_Confirm = 0,
    }

    public static class SceneName
    {
        public const string TITLE = "TitleScene";
        public const string GAME = "GameScene";
    }
}
