﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JH
{
    public class SceneChanger : SingletonMB<SceneChanger>
    {
        public void Change(string _sceneName)
        {
            var _scene = GameObject.FindObjectOfType<SceneBase>();
            if (null != _scene)
            {
                _scene.OnExit();
            }

            SceneLoader.Instance.LoadScene(_sceneName);
        }
    }
}

