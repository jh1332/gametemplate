﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace JH
{
    public abstract class PopupConfirmBase : PopupBase
    {
        [SerializeField] Button btnClose = null;
        [SerializeField] Button btnConfirm = null;

        protected bool closeAfterConfirmClick = true;

        protected override void OnAwake()
        {
            btnClose.onClick.AddListener(Close);

            if(null != btnConfirm)
            {
                btnConfirm.onClick.AddListener(HandleClick_Confirm);
            }
        }

        protected virtual void OnClick_Confirm(Action onEnd) { }

        void HandleClick_Confirm()
        {
            OnClick_Confirm(() =>
            {
                if (closeAfterConfirmClick)
                {
                    Close();
                }
            });
        }
    }
}



