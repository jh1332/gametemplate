using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace JH
{
	public class SceneLoader : MonoBehaviour
	{
		const string PATH = "Prefabs/SceneLoader";

		[SerializeField] CanvasGroup _sceneLoaderCanvasGroup = null;
		[SerializeField] Image _imgLoadingBar = null;
		[SerializeField] Text _textLoadingProgress = null;

		protected static SceneLoader _instance = null;
		public static SceneLoader Instance
		{
			get
			{
				if(_instance == null)
				{
					var obj = FindObjectOfType<SceneLoader>();
					if(obj != null)
					{
						_instance = obj;
					}
					else
					{
						_instance = Create();
					}
				}

				return _instance;
			}
			private set
			{
				_instance = value;
			}
		}

		string loadSceneName;

		public void LoadScene(string sceneName)
		{
			gameObject.SetActive(true);
			UnityEngine.SceneManagement.SceneManager.sceneLoaded += LoadSceneEnd;
			loadSceneName = sceneName; 
			StartCoroutine(Load(sceneName));
		}

		void Awake()
		{
			if(Instance != this)
			{
				Destroy(gameObject); return;
			}
			DontDestroyOnLoad(gameObject);
		}

		static SceneLoader Create()
		{
			var _sceneLoaderPrefab = Resources.Load<SceneLoader>(PATH);
			return Instantiate(_sceneLoaderPrefab);
		}

		IEnumerator Load(string sceneName)
		{
			_imgLoadingBar.fillAmount = 0f;
			_sceneLoaderCanvasGroup.alpha = 1;
			AsyncOperation _op = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName);
			_op.allowSceneActivation = false;
			float _timer = 0.0f;
			while(!_op.isDone)
			{
				yield return null; _timer += Time.unscaledDeltaTime;
				if(_op.progress < 0.9f)
				{
					_imgLoadingBar.fillAmount = Mathf.Lerp(_imgLoadingBar.fillAmount, _op.progress, _timer);
					if(_imgLoadingBar.fillAmount >= _op.progress)
					{
						_timer = 0f;
					}

					SetText_Progress(_op.progress);
				}
				else
				{
					_imgLoadingBar.fillAmount = Mathf.Lerp(_imgLoadingBar.fillAmount, 1f, _timer);
					if(_imgLoadingBar.fillAmount == 1.0f)
					{
						_op.allowSceneActivation = true;
						SetText_Progress(1);
						yield break;
					}

					SetText_Progress(_op.progress);
				}
			}
		}
		void LoadSceneEnd(UnityEngine.SceneManagement.Scene scene, UnityEngine.SceneManagement.LoadSceneMode loadSceneMode)
		{
			if(scene.name == loadSceneName)
			{
				StartCoroutine(Fade(false));
				UnityEngine.SceneManagement.SceneManager.sceneLoaded -= LoadSceneEnd;
			}
		}

		void SetText_Progress(float progress)
		{
			_textLoadingProgress.text = $"{progress * 100}%";
		}

		IEnumerator Fade(bool isFadeIn)
		{
			float _timer = 0f;
			while(_timer <= 1f)
			{
				yield return null;
				_timer += Time.deltaTime;
				_sceneLoaderCanvasGroup.alpha = Mathf.Lerp(isFadeIn ? 0 : 1, isFadeIn ? 1 : 0, _timer);
			}

			if(!isFadeIn)
			{
				gameObject.SetActive(false);
			}
		}
	}
}

