﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SceneTitle : SceneCommon
{
    [SerializeField] SceneUITitle sceneUI = null;

    int popupOrder = 0;

    protected override void OnEnter()
    {
        base.OnEnter();

        sceneUI.Init();

        
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();

        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            var popup = JH.UIManager.Instance.CreatePopup((int)Constants.PopupType.TestPopup_Confirm) as PopupConfirm;
            popup.name = $"{popup.name}_{popupOrder++}";
            popup.Set();
        }
    }
}
