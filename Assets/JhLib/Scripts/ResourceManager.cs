﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace JH
{
    public class ResourceManager : SingletonMB<ResourceManager>
    {
        bool _isInit = false;

        public void Init()
        {
            if (_isInit) { return; }

            _isInit = true;
        }

        public T Load<T>(string path) where T : UnityEngine.Object
        {
            return Resources.Load<T>(path);
        }

        public List<T> LoadAll<T>(string path) where T : UnityEngine.Object
        {
            return new List<T>(Resources.LoadAll<T>(path));
        }
    }
}

