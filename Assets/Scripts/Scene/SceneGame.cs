﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneGame : SceneCommon
{
    [SerializeField] SceneUIGame sceneUI = null;

    protected override void OnEnter()
    {
        base.OnEnter();

        sceneUI.Init();
    }
}
