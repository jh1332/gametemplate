﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

namespace JH
{
    public abstract class PopupBase : MonoBehaviour
    {
        public abstract int GetPopupType();

        protected virtual void OnAwake() { }
        protected virtual void OnUpdate() { }

        protected void Open()
        {
            JH.UIManager.Instance.TurnOnBlock();
            ActOpening(() => { JH.UIManager.Instance.TurnOffBlock(); });
        }

        protected void ActOpening()
        {
            ActOpening(() => { });
        }

        protected void ActOpening(Action onEnd)
        {
            transform.localScale = Vector3.zero;
            Sequence openingSequence = DOTween.Sequence();
            openingSequence.Append(transform.DOScale(1.3f, 0.3f));
            openingSequence.Append(transform.DOScale(1, 0.2f));
            openingSequence.AppendCallback(() => 
            {
                onEnd();
            });
        }

        protected void Close()
        {
            JH.UIManager.Instance.TurnOnBlock();
            ActClosing(() =>
            {
                JH.UIManager.Instance.DestroyPopup(this);
                JH.UIManager.Instance.TurnOffBlock();
            });
        }

        protected void ActClosing(Action onEnd)
        {
            Sequence openingSequence = DOTween.Sequence();
            openingSequence.Append(transform.DOScale(0, 0.2f));
            openingSequence.AppendCallback(() => 
            {
                onEnd();
            });
        }

        void Awake()
        {
            OnAwake();    
        }

        void Update()
        {
            OnUpdate();
        }
    }
}

