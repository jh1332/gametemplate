﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace JH
{
    public abstract class SceneBase : MonoBehaviour
    {
        
        protected virtual void OnEnter()
        {
            InitDataManager(()=>
            {
                InitResourceManager();
                InitUIManager();
            });
        }

        public virtual void OnExit()
        {
            ObjectPool.AllClear();
            Resources.UnloadUnusedAssets();
            System.GC.Collect();
        }

        protected abstract void InitUIManager();
        protected abstract void InitDataManager(Action _onEnd);

        protected virtual void OnUpdate() { }


        void Awake()
        {
            OnEnter();
        }

        void Update()
        {
            OnUpdate();
        }

        void InitResourceManager()
        {
            ResourceManager.Instance.Init();
        }
    }
}

