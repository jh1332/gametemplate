﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace JH
{
    public abstract class DataManagerBase<T> : SingletonMB<T> where T : MonoBehaviour
    {
        bool _isInit = false;

        public void Init(Action onEnd)
        {
            if (_isInit) { onEnd(); return; }

            OnInit(()=>
            {
                _isInit = true;
                onEnd();
            });
        }

        protected abstract void OnInit(Action _onEnd);
    }
}
