﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PopupConfirm : JH.PopupConfirmBase
{
    public override int GetPopupType()
    {
        return (int)Constants.PopupType.TestPopup_Confirm;
    }

    public void Set()
    {
        Open();
    }

    protected override void OnClick_Confirm(Action onEnd)
    {
        Debug.Log("확인버튼 클릭");
        onEnd();
    }
}
