﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    private void Awake()
    {
        JH.Event.Subscribe(gameObject, (int)Constants.EventType.TestEvent_0, OnTestEvent_0);
        JH.Event.Subscribe(gameObject, (int)Constants.EventType.TestEvent_1, OnTestEvent_1);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            JH.Event.OnNext((int)Constants.EventType.TestEvent_0);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            JH.Event.OnNext((int)Constants.EventType.TestEvent_1, 10);
        }
    }

    void OnTestEvent_0(int eventType, object[] args)
    {
        Debug.Log($"OnTestEvent_0 - eventType: {(Constants.EventType)eventType}");
    }

    void OnTestEvent_1(int eventType, object[] args)
    {
        Debug.Log($"OnTestEvent_1 - eventType: {(Constants.EventType)eventType}, args: {(int)args[0]}");
    }
}
