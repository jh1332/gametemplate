﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SceneCommon : JH.SceneBase
{
    protected override void InitUIManager()
    {
        JH.UIManager.Instance.Init();
    }

    protected override void InitDataManager(Action _onEnd)
    {
        DataManager.Instance.Init(_onEnd);
    }
}
