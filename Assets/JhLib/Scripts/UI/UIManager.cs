﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace JH
{
    public partial class UIManager : SingletonMB_NoDontDestroy<UIManager>
    {
        static Dictionary<int, GameObject> _loadedPopupPrefabs = new Dictionary<int, GameObject>();
        Canvas _popupCanvas = null;
        GameObject block = null;
        List<PopupBase> _openedPopups = new List<PopupBase>();

        public void Init()
        {
            CreatePopupCanvas();
            CreateBlock();
            InitPopupPrefabs();
        }

        public PopupBase CreatePopup(int popupType)
        {
            var popup = InstantiatePopup(popupType);
            SetPopupDepth(popup);
            PushPopup(popup);

            return popup;
        }

        public void DestroyPopup(PopupBase popup)
        {
            _openedPopups.Remove(popup);
            popup.Recycle();
        }

        public void TurnOnBlock()
        {
            block.SetActive(true);
        }

        public void TurnOffBlock()
        {
            block.SetActive(false);
        }

        void CreatePopupCanvas()
        {
            _popupCanvas = CreateCanvas("Popup Canvas", 1);
        }

        void CreateBlock()
        {
            Canvas blockCanvas = CreateCanvas("Block Canvas", 100);

            // Image 생성
            GameObject blockGO = new GameObject("Block Image");
            Image blockImage = blockGO.AddComponent<Image>();
            Color imageColor = blockImage.color;
            imageColor.a = 0;
            blockImage.color = imageColor;
            blockImage.rectTransform.sizeDelta = new Vector2(3000, 3000);

            blockGO.transform.SetParent(blockCanvas.transform);
            blockGO.transform.localPosition = Vector3.zero;

            // 
            block = blockCanvas.gameObject;
            TurnOffBlock();
        }

        Canvas CreateCanvas(string name, int sortingOrder)
        {
            var mainCanvas = GameObject.FindObjectOfType<Canvas>();
            var mainCanvasScaler = mainCanvas.GetComponent<CanvasScaler>();

            GameObject canvasGO = new GameObject(name);
            Canvas canvas = canvasGO.AddComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            canvas.sortingOrder = sortingOrder;

            // Canvas Scaler
            var canvasScaler = canvas.gameObject.AddComponent<CanvasScaler>();
            canvasScaler.uiScaleMode = mainCanvasScaler.uiScaleMode;
            canvasScaler.scaleFactor = mainCanvasScaler.scaleFactor;
            canvasScaler.referencePixelsPerUnit = mainCanvasScaler.referencePixelsPerUnit;

            // Graphic Raycaster
            canvas.gameObject.AddComponent<GraphicRaycaster>();

            return canvas;
        }

        void InitPopupPrefabs()
        {
            LoadPopupPrefabs();

            foreach(var p in _loadedPopupPrefabs)
            {
                p.Value.CreatePool(1);
            }
        }

        void LoadPopupPrefabs()
        {
            if (0 < _loadedPopupPrefabs.Count) { return; }

            string rootPath = GetPopupPrefabRootPath();
            var prefabList = ResourceManager.Instance.LoadAll<GameObject>(rootPath);
            prefabList.ForEach(prefab => 
            {
                var popup = prefab.GetComponent<PopupBase>();
                if(null != popup)
                {
                    _loadedPopupPrefabs.Add(popup.GetPopupType(), prefab);
                }
            });           
        }

        PopupBase InstantiatePopup(int popupType)
        {
            return _loadedPopupPrefabs[popupType].Spawn().GetComponent<PopupBase>();
        }

        void SetPopupDepth(PopupBase popup)
        {
            popup.transform.SetParent(_popupCanvas.transform);
            popup.transform.localPosition = Vector3.zero;
            popup.transform.localRotation = Quaternion.identity;
        }

        void PushPopup(PopupBase popup)
        {
            _openedPopups.Add(popup);
        }
    }
}
