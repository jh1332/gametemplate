﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneUIGame : SceneCommon
{
    [SerializeField] Button btnGoToTitle = null;
    [SerializeField] Button btnTest_1 = null;
    [SerializeField] Button btnTest_2 = null;

    public void Init()
    {

    }

    private void Awake()
    {
        btnGoToTitle.onClick.AddListener(() => { JH.SceneChanger.Instance.Change(Constants.SceneName.TITLE); });
        btnTest_1.onClick.AddListener(() => { JH.UIManager.Instance.CreatePopup((int)Constants.PopupType.TestPopup_Confirm); });
    }
}
