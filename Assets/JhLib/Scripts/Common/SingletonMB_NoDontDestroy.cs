﻿using UnityEngine;
using UnityObject = UnityEngine.Object;
using UnityRandom = UnityEngine.Random;
#if UNITY_WEBGL && !UNITY_EDITOR
using D = Unit5.WebPlayerDebug;
#else
using D = UnityEngine.Debug;
#endif

using System;
using System.Collections;
using System.Collections.Generic;

//----------
namespace JH
{

	//----------
	public class SingletonMB_NoDontDestroy<T> : MonoBehaviour where T : MonoBehaviour
	{

		//----------
		static readonly object _syncRoot = new System.Object();

		static protected volatile T _instance;

		//----------
		static public T Instance
		{
			get
			{
				if(_instance == null)
				{
					lock(_syncRoot)
					{
						_instance = (T)FindObjectOfType(typeof(T));

						if(FindObjectsOfType(typeof(T)).Length > 1)
						{
							Debug.LogError("[SingletonMB] Something went really wrong  - there should never be more than 1 singleton! Reopening the scene might fix it.");
							return _instance;
						}

						if(_instance == null)
						{
							//string tName = "(singleton) " + typeof(T).ToString();
							string tName = typeof(T).ToString();
							GameObject go = new GameObject("_" + tName);
							_instance = go.AddComponent<T>();
						}
					}
				}
				return _instance;
			}
		}

		static public bool IsExistInstance()
		{
			return (_instance != null);
		}
	}

}