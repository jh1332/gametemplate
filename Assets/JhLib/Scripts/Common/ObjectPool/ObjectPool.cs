﻿
using UnityEngine;
using UnityObject = UnityEngine.Object;
using UnityRandom = UnityEngine.Random;
#if UNITY_WEBGL && !UNITY_EDITOR
using D = Unit5.WebPlayerDebug;
#else
using D = UnityEngine.Debug;
#endif

using System;
using System.Collections;
using System.Collections.Generic;

//----------
namespace JH
{

	//----------
	sealed public class ObjectPool : SingletonMB_NoDontDestroy<ObjectPool>
	{

		//----------
		public enum StartupPoolMode
		{
			Awake, Start, CallManually
		};

		//----------
		[Serializable]
		public class StartupPool
		{
			public int size;
			public GameObject prefab;
		}

		//----------
		static List<GameObject> recycleTempList = new List<GameObject>();

		//----------
		public StartupPoolMode startupPoolMode;
		public StartupPool[] startupPools;

		//----------
		bool startupPoolsCreated;

		Dictionary<GameObject, List<GameObject>> pooledObjects = new Dictionary<GameObject, List<GameObject>>();
		Dictionary<GameObject, GameObject> spawnedObjects = new Dictionary<GameObject, GameObject>();

		public void Init()
		{
		}

		//----------
		void Awake()
		{
			if(startupPoolMode == StartupPoolMode.Awake)
			{
				CreateStartupPools();
			}
		}

		void Start()
		{
			if(startupPoolMode == StartupPoolMode.Start)
			{
				CreateStartupPools();
			}
		}

		//----------
		static public void CreateStartupPools()
		{
			if(!Instance.startupPoolsCreated)
			{
				Instance.startupPoolsCreated = true;

				var pools = Instance.startupPools;
				if(pools != null && pools.Length > 0)
				{
					for(int i = 0; i < pools.Length; i += 1)
					{
						CreatePool(pools[i].prefab, pools[i].size);
					}
				}
			}
		}

		//----------
		static public void CreatePool<T>(T prefab, int initialPoolSize) where T : Component
		{
			CreatePool(prefab.gameObject, initialPoolSize);
		}

		static public ObjectPoolInstance<T> CreateObjectPoolInstance<T>(GameObject prefab, int initialPoolSize) where T : MonoBehaviour
		{
			CreatePool(prefab.gameObject, initialPoolSize);
			return new ObjectPoolInstance<T>(prefab);
		}

		static public void CreatePool(GameObject prefab, int initialPoolSize)
		{
			if(prefab != null && !Instance.pooledObjects.ContainsKey(prefab))
			{
				string prefabName = prefab.name;

				var list = new List<GameObject>();
				Instance.pooledObjects.Add(prefab, list);

				if(initialPoolSize > 0)
				{
					Transform parent = Instance.transform;
					while(list.Count < initialPoolSize)
					{
						var obj = (GameObject)UnityObject.Instantiate(prefab);
						obj.name = prefabName;
						obj.transform.SetParent(parent);
						//obj.transform.parent = parent;
						obj.SetActive(false);
						list.Add(obj);
					}
				}
			}
		}
		static public void CreatePool_Sound(GameObject prefab, int initialPoolSize)
		{
			CreatePool(prefab, initialPoolSize);

			AudioSource audio = null;
			List<GameObject> list = Instance.pooledObjects[prefab];
			for(int i = 0; i < list.Count; ++i)
			{
				audio = list[i].GetComponent<AudioSource>();
				if(null != audio)
				{
					audio.clip.LoadAudioData();
				}
			}
		}

		//----------
		static public void ClearPool(GameObject prefab, bool unloadPrefab = false)
		{
			prefab.RecycleAll();
			if(!Instance.pooledObjects.ContainsKey(prefab)) return;

			if(unloadPrefab)
			{
				foreach(var go in Instance.pooledObjects[prefab]) GameObject.DestroyImmediate(go);
				Instance.pooledObjects[prefab].Clear();
				Instance.pooledObjects.Remove(prefab);

				/** /
				#pragma warning disable 0168
				try
				{
					Resources.UnloadAsset(prefab);
				}
				catch(Exception e)
				{
					// Debug.LogError(e);
				}
				#pragma warning restore 0168
				/**/

				Resources.UnloadUnusedAssets();
			}
			else
			{
				foreach(var go in Instance.pooledObjects[prefab]) GameObject.Destroy(go);
				Instance.pooledObjects[prefab].Clear();
				Instance.pooledObjects.Remove(prefab);
			}
		}

		//----------
		static public void AllClear()
		{
			foreach(var goList in Instance.pooledObjects.Values)
			{
				foreach(var go in goList) GameObject.Destroy(go);
				goList.Clear();
			}
			Instance.pooledObjects.Clear();
		}

		//----------
		static public T Spawn<T>(T prefab) where T : Component
		{
			return Spawn(prefab.gameObject, null, Vector3.zero, Quaternion.identity).GetComponent<T>();
		}
		static public T Spawn<T>(T prefab, Vector3 position) where T : Component
		{
			return Spawn(prefab.gameObject, null, position, Quaternion.identity).GetComponent<T>();
		}
		static public T Spawn<T>(T prefab, Vector3 position, Vector3 rotation) where T : Component
		{
			return Spawn(prefab.gameObject, null, position, Quaternion.Euler(rotation.x, rotation.y, rotation.z)).GetComponent<T>();
		}
		static public T Spawn<T>(T prefab, Vector3 position, Quaternion rotation) where T : Component
		{
			return Spawn(prefab.gameObject, null, position, rotation).GetComponent<T>();
		}

		static public T Spawn<T>(T prefab, Transform parent) where T : Component
		{
			return Spawn(prefab.gameObject, parent, Vector3.zero, Quaternion.identity).GetComponent<T>();
		}
		static public T Spawn<T>(T prefab, Transform parent, Vector3 position) where T : Component
		{
			return Spawn(prefab.gameObject, parent, position, Quaternion.identity).GetComponent<T>();
		}
		static public T Spawn<T>(T prefab, Transform parent, Vector3 position, Vector3 rotation) where T : Component
		{
			return Spawn(prefab.gameObject, parent, position, Quaternion.Euler(rotation.x, rotation.y, rotation.z)).GetComponent<T>();
		}
		static public T Spawn<T>(T prefab, Transform parent, Vector3 position, Quaternion rotation) where T : Component
		{
			return Spawn(prefab.gameObject, parent, position, rotation).GetComponent<T>();
		}

		static public GameObject Spawn(GameObject prefab)
		{
			return Spawn(prefab, null, Vector3.zero, Quaternion.identity);
		}
		static public GameObject Spawn(GameObject prefab, Vector3 position)
		{
			return Spawn(prefab, null, position, Quaternion.identity);
		}
		static public GameObject Spawn(GameObject prefab, Vector3 position, Vector3 rotation)
		{
			return Spawn(prefab, null, position, Quaternion.Euler(rotation.x, rotation.y, rotation.z));
		}
		static public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation)
		{
			return Spawn(prefab, null, position, rotation);
		}

		static public GameObject Spawn(GameObject prefab, Transform parent)
		{
			return Spawn(prefab, parent, Vector3.zero, Quaternion.identity);
		}
		static public GameObject Spawn(GameObject prefab, Transform parent, Vector3 position)
		{
			return Spawn(prefab, parent, position, Quaternion.identity);
		}
		static public GameObject Spawn(GameObject prefab, Transform parent, Vector3 position, Vector3 rotation)
		{
			return Spawn(prefab, parent, position, Quaternion.Euler(rotation.x, rotation.y, rotation.z));
		}
		static public GameObject Spawn(GameObject prefab, Transform parent, Vector3 position, Quaternion rotation)
		{
			List<GameObject> list;
			Transform trans;
			GameObject obj;
			string prefabName = prefab.name;
			if(Instance.pooledObjects.TryGetValue(prefab, out list))
			{
				obj = null;
				if(list.Count > 0)
				{
					while(obj == null && list.Count > 0)
					{
						obj = list[0];
						list.RemoveAt(0);
					}

					if(obj != null)
					{						
						trans = obj.transform;
						trans.SetParent(parent);
						//trans.parent = parent;
						trans.localPosition = position;
						trans.localRotation = rotation;
						obj.SetActive(true);
						Instance.spawnedObjects.Add(obj, prefab);
						return obj;
					}
				}

				obj = (GameObject)UnityObject.Instantiate(prefab);
				obj.name = prefabName;
				trans = obj.transform;
				trans.SetParent(parent);
				//trans.parent = parent;
				trans.localPosition = position;
				trans.localRotation = rotation;
				Instance.spawnedObjects.Add(obj, prefab);
				return obj;
			}
			else
			{
				obj = (GameObject)UnityObject.Instantiate(prefab);
				obj.name = prefabName;
				trans = obj.GetComponent<Transform>();
				trans.parent = parent;
				trans.localPosition = position;
				trans.localRotation = rotation;
				return obj;
			}
		}

		//----------
		static public void Recycle<T>(T component) where T : Component
		{
			Recycle(component.gameObject);
		}
		static public void Recycle(GameObject gameObject)
		{
			if(false == IsSpawned(gameObject))
			{
				gameObject.SetActive(false);
				return;
			}

			GameObject prefab;
			if(Instance.spawnedObjects.TryGetValue(gameObject, out prefab))
			{
				Recycle(gameObject, prefab);
			}
			else
			{
				gameObject.SetActive(false);
			}
		}
		static void Recycle(GameObject gameObject, GameObject prefab)
		{
			if(false == Instance.pooledObjects.ContainsKey(prefab)) return;

			Instance.pooledObjects[prefab].Add(gameObject);
			Instance.spawnedObjects.Remove(gameObject);

			// 크기를 유지한채로 ObjectPool의 자식으로 붙임
			Vector3 localScale = gameObject.transform.localScale;
			gameObject.transform.SetParent(Instance.transform);
			//gameObject.transform.parent = Instance.transform;
			gameObject.transform.localScale = localScale;

			gameObject.SetActive(false);
		}

		static public void RecycleAll<T>(T prefab) where T : Component
		{
			RecycleAll(prefab.gameObject);
		}
		static public void RecycleAll(GameObject prefab)
		{
			foreach(var item in Instance.spawnedObjects)
			{
				if(item.Value == prefab) recycleTempList.Add(item.Key);
			}
			for(int i = 0; i < recycleTempList.Count; i += 1)
			{
				Recycle(recycleTempList[i]);
			}
			recycleTempList.Clear();
		}
		static public void RecycleAll()
		{
			recycleTempList.AddRange(Instance.spawnedObjects.Keys);
			for(int i = 0; i < recycleTempList.Count; i += 1)
			{
				Recycle(recycleTempList[i]);
			}
			recycleTempList.Clear();
		}

		//----------
		static public bool IsSpawned(GameObject gameObject)
		{
			return Instance.spawnedObjects.ContainsKey(gameObject);
		}

		//----------
		static public int CountPooled<T>(T prefab) where T : Component
		{
			return CountPooled(prefab.gameObject);
		}
		static public int CountPooled(GameObject prefab)
		{
			List<GameObject> list;
			if(Instance.pooledObjects.TryGetValue(prefab, out list)) return list.Count;
			return 0;
		}

		static public int CountAllPooled()
		{
			int count = 0;
			foreach(var list in Instance.pooledObjects.Values) count += list.Count;
			return count;
		}

		static public int CountSpawned<T>(T prefab) where T : Component
		{
			return CountSpawned(prefab.gameObject);
		}
		static public int CountSpawned(GameObject prefab)
		{
			int count = 0;
			foreach(var instancePrefab in Instance.spawnedObjects.Values)
			{
				if(prefab == instancePrefab) count += 1;
			}
			return count;
		}

		static public int CountAllSpawned()
		{
			return Instance.spawnedObjects.Count;
		}

		//----------
		static public List<T> GetPooled<T>(T prefab, List<T> list, bool appendList) where T : Component
		{
			if(list == null) list = new List<T>();
			if(!appendList) list.Clear();

			List<GameObject> pooled;
			if(Instance.pooledObjects.TryGetValue(prefab.gameObject, out pooled))
			{
				for(int i = 0; i < pooled.Count; i += 1)
				{
					list.Add(pooled[i].GetComponent<T>());
				}
			}
			return list;
		}
		static public List<GameObject> GetPooled(GameObject prefab, List<GameObject> list, bool appendList)
		{
			if(list == null) list = new List<GameObject>();
			if(!appendList) list.Clear();

			List<GameObject> pooled;
			if(Instance.pooledObjects.TryGetValue(prefab, out pooled))
			{
				list.AddRange(pooled);
			}
			return list;
		}

		//----------
		static public List<T> GetSpawned<T>(T prefab, List<T> list, bool appendList) where T : Component
		{
			if(list == null) list = new List<T>();
			if(!appendList) list.Clear();

			var prefabObj = prefab.gameObject;
			foreach(var item in Instance.spawnedObjects)
			{
				if(item.Value == prefabObj) list.Add(item.Key.GetComponent<T>());
			}
			return list;
		}
		static public List<GameObject> GetSpawned(GameObject prefab, List<GameObject> list, bool appendList)
		{
			if(list == null) list = new List<GameObject>();
			if(!appendList) list.Clear();

			foreach(var item in Instance.spawnedObjects)
			{
				if(item.Value == prefab) list.Add(item.Key);
			}
			return list;
		}
	}

}