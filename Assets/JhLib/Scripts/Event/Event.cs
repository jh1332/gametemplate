﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace JH
{
    public static class Event
    {
        static Subject<EventInfo> _rxEvent = new Subject<EventInfo>();

        public static IDisposable Subscribe(GameObject disposer, int eventType, Action<int, object[]> onEvent)
        {
            return _rxEvent
                .Where(e => e._eventType == eventType)
                .Subscribe(e =>
                {
                    onEvent(e._eventType, e._args);
                }).AddTo(disposer);
        }

        public static void OnNext(int eventType, params object[] args)
        {
            _rxEvent.OnNext(new EventInfo(eventType, args));
        }

        class EventInfo
        {
            public object[] _args;
            public int _eventType;

            public EventInfo(int eventType, params object[] args)
            {
                _eventType = eventType;
                _args = args;
            }
        }
    }
}
