﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JH
{
    public abstract class RequestBase
    {
        public abstract void Run();
    }
}

