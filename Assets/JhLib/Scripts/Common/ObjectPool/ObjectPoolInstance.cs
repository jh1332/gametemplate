﻿using UnityEngine;
using UnityObject = UnityEngine.Object;
using UnityRandom = UnityEngine.Random;
#if UNITY_WEBGL && !UNITY_EDITOR
using D = Unit5.WebPlayerDebug;
#else
using D = UnityEngine.Debug;
#endif

using System;
using System.Collections;
using System.Collections.Generic;

//----------
namespace JH
{

	//----------
	public class ObjectPoolInstance<T> where T : MonoBehaviour
	{

		//----------
		GameObject prefab;

		Dictionary<GameObject, T> components = new Dictionary<GameObject, T>();

		//----------
		public ObjectPoolInstance(GameObject prefab)
		{
			this.prefab = prefab;

			List<T> componentList = prefab.GetComponent<T>().GetPooled();
			for(int i = 0; i < componentList.Count; i += 1)
			{
				components.Add(componentList[i].gameObject, componentList[i]);
			}
		}

		//----------
		public T Spawn()
		{
			return GetComponent(ObjectPool.Spawn(prefab, null, Vector3.zero, Quaternion.identity));
		}
		public T Spawn(Vector3 pos)
		{
			return GetComponent(ObjectPool.Spawn(prefab, null, pos, Quaternion.identity));
		}
		public T Spawn(Vector3 pos, Vector3 rot)
		{
			return GetComponent(ObjectPool.Spawn(prefab, null, pos, Quaternion.Euler(rot.x, rot.y, rot.z)));
		}
		public T Spawn(Vector3 pos, Quaternion rot)
		{
			return GetComponent(ObjectPool.Spawn(prefab, null, pos, rot));
		}
		public T Spawn(Transform parent)
		{
			return GetComponent(ObjectPool.Spawn(prefab, parent, Vector3.zero, Quaternion.identity));
		}
		public T Spawn(Transform parent, Vector3 pos)
		{
			return GetComponent(ObjectPool.Spawn(prefab, parent, pos, Quaternion.identity));
		}
		public T Spawn(Transform parent, Vector3 pos, Vector3 rot)
		{
			return GetComponent(ObjectPool.Spawn(prefab, parent, pos, Quaternion.Euler(rot.x, rot.y, rot.z)));
		}
		public T Spawn(Transform parent, Vector3 pos, Quaternion rot)
		{
			return GetComponent(ObjectPool.Spawn(prefab, parent, pos, rot));
		}

		//----------
		T GetComponent(GameObject spawned)
		{
			if(false == components.ContainsKey(spawned))
			{
				components.Add(spawned, spawned.GetComponent<T>());
			}
			return components[spawned];
		}
	}

}