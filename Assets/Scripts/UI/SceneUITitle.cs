﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneUITitle : MonoBehaviour
{
    [SerializeField] Button btnOK = null;

    public void Init()
    {

    }

    private void Awake()
    {
        btnOK.onClick.AddListener(() =>
        {
            JH.SceneChanger.Instance.Change(Constants.SceneName.GAME);
        });
    }
}
