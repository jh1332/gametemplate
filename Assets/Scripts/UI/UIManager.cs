﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JH
{
    public partial class UIManager
    {
        const string POPUP_PREFAB_PATH = "Prefabs/Popups";

        string GetPopupPrefabRootPath()
        {
            return POPUP_PREFAB_PATH;
        }
    }
}

