﻿using UnityEngine;
using UnityObject = UnityEngine.Object;
using UnityRandom = UnityEngine.Random;
#if UNITY_WEBGL && !UNITY_EDITOR
using D = Unit5.WebPlayerDebug;
#else
using D = UnityEngine.Debug;
#endif

using System;
using System.Collections;
using System.Collections.Generic;

//----------
namespace JH
{

	//----------
	static public class ObjectPoolExtensions
	{

		//----------
		static public void CreatePool<T>(this T prefab) where T : Component
		{
			ObjectPool.CreatePool(prefab, 0);
		}
		static public void CreatePool<T>(this T prefab, int initialPoolSize) where T : Component
		{
			ObjectPool.CreatePool(prefab, initialPoolSize);
		}

		static public void CreatePool(this GameObject prefab)
		{
			ObjectPool.CreatePool(prefab, 0);
		}
		static public void CreatePool(this GameObject prefab, int initialPoolSize)
		{
			ObjectPool.CreatePool(prefab, initialPoolSize);
		}
		static public void CreatePool_Sound(this GameObject prefab, int initialPoolSize)
		{
			ObjectPool.CreatePool_Sound(prefab, initialPoolSize);
		}

		static public ObjectPoolInstance<T> CreateObjectPoolInstance<T>(this GameObject prefab, int initialPoolSize) where T : MonoBehaviour
		{
			return ObjectPool.CreateObjectPoolInstance<T>(prefab, initialPoolSize);
		}

		//----------
		static public void ClearPool(this GameObject prefab, bool unloadPrefab = false)
		{
			ObjectPool.ClearPool(prefab, unloadPrefab);
		}

		//----------
		static public T Spawn<T>(this T prefab) where T : Component
		{
			return ObjectPool.Spawn(prefab, null, Vector3.zero, Quaternion.identity);
		}
		static public T Spawn<T>(this T prefab, Vector3 position) where T : Component
		{
			return ObjectPool.Spawn(prefab, null, position, Quaternion.identity);
		}
		static public T Spawn<T>(this T prefab, Vector3 position, Vector3 rotation) where T : Component
		{
			return ObjectPool.Spawn(prefab, null, position, Quaternion.Euler(rotation.x, rotation.y, rotation.z));
		}
		static public T Spawn<T>(this T prefab, Vector3 position, Quaternion rotation) where T : Component
		{
			return ObjectPool.Spawn(prefab, null, position, rotation);
		}

		static public T Spawn<T>(this T prefab, Transform parent) where T : Component
		{
			return ObjectPool.Spawn(prefab, parent, Vector3.zero, Quaternion.identity);
		}
		static public T Spawn<T>(this T prefab, Transform parent, Vector3 position) where T : Component
		{
			return ObjectPool.Spawn(prefab, parent, position, Quaternion.identity);
		}
		static public T Spawn<T>(this T prefab, Transform parent, Vector3 position, Vector3 rotation) where T : Component
		{
			return ObjectPool.Spawn(prefab, parent, position, Quaternion.Euler(rotation.x, rotation.y, rotation.z));
		}
		static public T Spawn<T>(this T prefab, Transform parent, Vector3 position, Quaternion rotation) where T : Component
		{
			return ObjectPool.Spawn(prefab, parent, position, rotation);
		}

		static public GameObject Spawn(this GameObject prefab)
		{
			return ObjectPool.Spawn(prefab, null, Vector3.zero, Quaternion.identity);
		}
		static public GameObject Spawn(this GameObject prefab, Vector3 position)
		{
			return ObjectPool.Spawn(prefab, null, position, Quaternion.identity);
		}
		static public GameObject Spawn(this GameObject prefab, Vector3 position, Vector3 rotation)
		{
			return ObjectPool.Spawn(prefab, null, position, Quaternion.Euler(rotation.x, rotation.y, rotation.z));
		}
		static public GameObject Spawn(this GameObject prefab, Vector3 position, Quaternion rotation)
		{
			return ObjectPool.Spawn(prefab, null, position, rotation);
		}

		static public GameObject Spawn(this GameObject prefab, Transform parent)
		{
			return ObjectPool.Spawn(prefab, parent, Vector3.zero, Quaternion.identity);
		}
		static public GameObject Spawn(this GameObject prefab, Transform parent, Vector3 position)
		{
			return ObjectPool.Spawn(prefab, parent, position, Quaternion.identity);
		}
		static public GameObject Spawn(this GameObject prefab, Transform parent, Vector3 position, Vector3 rotation)
		{
			return ObjectPool.Spawn(prefab, parent, position, Quaternion.Euler(rotation.x, rotation.y, rotation.z));
		}
		static public GameObject Spawn(this GameObject prefab, Transform parent, Vector3 position, Quaternion rotation)
		{
			return ObjectPool.Spawn(prefab, parent, position, rotation);
		}

		//----------
		static public void Recycle<T>(this T component) where T : Component
		{
			ObjectPool.Recycle(component);
		}
		static public void RecycleAll<T>(this T prefab) where T : Component
		{
			ObjectPool.RecycleAll(prefab);
		}

		static public void Recycle(this GameObject gameObject)
		{
			ObjectPool.Recycle(gameObject);
		}
		static public void RecycleAll(this GameObject prefab)
		{
			ObjectPool.RecycleAll(prefab);
		}

		//----------
		static public int CountPooled<T>(this T prefab) where T : Component
		{
			return ObjectPool.CountPooled(prefab);
		}
		static public int CountPooled(this GameObject prefab)
		{
			return ObjectPool.CountPooled(prefab);
		}

		static public int CountSpawned<T>(this T prefab) where T : Component
		{
			return ObjectPool.CountSpawned(prefab);
		}
		static public int CountSpawned(this GameObject prefab)
		{
			return ObjectPool.CountSpawned(prefab);
		}

		//----------
		static public List<T> GetPooled<T>(this T prefab) where T : Component
		{
			return ObjectPool.GetPooled(prefab, null, false);
		}
		static public List<T> GetPooled<T>(this T prefab, List<T> list) where T : Component
		{
			return ObjectPool.GetPooled(prefab, list, false);
		}
		static public List<T> GetPooled<T>(this T prefab, List<T> list, bool appendList) where T : Component
		{
			return ObjectPool.GetPooled(prefab, list, appendList);
		}

		static public List<GameObject> GetPooled(this GameObject prefab)
		{
			return ObjectPool.GetPooled(prefab, null, false);
		}
		static public List<GameObject> GetPooled(this GameObject prefab, List<GameObject> list)
		{
			return ObjectPool.GetPooled(prefab, list, false);
		}
		static public List<GameObject> GetPooled(this GameObject prefab, List<GameObject> list, bool appendList)
		{
			return ObjectPool.GetPooled(prefab, list, appendList);
		}

		//----------
		static public List<T> GetSpawned<T>(this T prefab) where T : Component
		{
			return ObjectPool.GetSpawned(prefab, null, false);
		}
		static public List<T> GetSpawned<T>(this T prefab, List<T> list) where T : Component
		{
			return ObjectPool.GetSpawned(prefab, list, false);
		}
		static public List<T> GetSpawned<T>(this T prefab, List<T> list, bool appendList) where T : Component
		{
			return ObjectPool.GetSpawned(prefab, list, appendList);
		}

		static public List<GameObject> GetSpawned(this GameObject prefab)
		{
			return ObjectPool.GetSpawned(prefab, null, false);
		}
		static public List<GameObject> GetSpawned(this GameObject prefab, List<GameObject> list)
		{
			return ObjectPool.GetSpawned(prefab, list, false);
		}
		static public List<GameObject> GetSpawned(this GameObject prefab, List<GameObject> list, bool appendList)
		{
			return ObjectPool.GetSpawned(prefab, list, appendList);
		}
	}

}